//
//  keyboardTests.swift
//  keyboardTests
//
//  Created by Jan Plesek on 28/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import XCTest
@testable import calc

class keyboardTests: XCTestCase {
	
	
	var keyboard	: KeyboardView!
	var output		: Character!
	let wait		= DispatchGroup()
	
	override func setUp() {
		keyboard = KeyboardView(clickBlock: { (character) in
			self.output = character
			self.wait.leave()
		})
		keyboard.changeOrientation(.portrait)
	}

	
	func testKey0()			{	test(clickButton: "0", "0")		}	// pressing button
	func testKey1()			{	test(clickButton: "1", "1")		}	// pressing button
	func testKey2()			{	test(clickButton: "2", "2")		}	// pressing button
	func testKey3()			{	test(clickButton: "3", "3")		}	// pressing button
	func testKey4()			{	test(clickButton: "4", "4")		}	// pressing button
	func testKey5()			{	test(clickButton: "5", "5")		}	// pressing button
	func testKey6()			{	test(clickButton: "6", "6")		}	// pressing button
	func testKey7()			{	test(clickButton: "7", "7")		}	// pressing button
	func testKey8()			{	test(clickButton: "8", "8")		}	// pressing button
	func testKey9()			{	test(clickButton: "9", "9")		}	// pressing button
	func testKeyDot()		{	test(clickButton: ".", ".")		}	// pressing button
	func testKeyPlus()		{	test(clickButton: "+", "+")		}	// pressing button
	func testKeyMinus()		{	test(clickButton: "-", "-")		}	// pressing button
	func testKeyStar()		{	test(clickButton: "*", "*")		}	// pressing button
	func testKeySlash()		{	test(clickButton: "/", "/")		}	// pressing button
	func testKeyEqual()		{	test(clickButton: "=", "=")		}	// pressing button
	func testKeyClearAll()	{	test(clickButton: "A", "A")		}	// pressing button
	func testKeyClear()		{										// changing and pressing button
		wait.enter()
		keyboard.changeKey(from: "A", to: "C")
		test(clickButton: "C", "C")
	}

	
	private func test(clickButton: Character, _ expectedOutput: Character) {
		
		// GIVEN
		wait.enter()
		
		
		// WHEN
		if(!keyboard.fakeClick(to: clickButton)){
			XCTFail()	//button doesn't exists
		}
		
		
		// THEN
		wait.notify(queue: .main) {
			XCTAssertEqual(self.output, expectedOutput)
		}
		
	}
}
