//
//  processorTests.swift
//  processorTests
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import XCTest
@testable import calc

class processorTests: XCTestCase {

	
	var processor : Processor!
	
    override func setUp() {
        processor = Processor(formatter: .testing)
    }
	
	//input
	func testComputationA01() {	test( "0",			"0")		}	//input
	func testComputationA02() {	test( "1",			"1")		}	//input
	func testComputationA03() {	test( "=",			"0")		}	//input
	func testComputationA04() {	test( "+",			"0")		}	//input
	func testComputationA05() {	test( "-",			"0")		}	//input
	func testComputationA06() {	test( "C",			"0")		}	//input
	func testComputationA07() {	test( "0.0001",		"0.0001")	}	//input
	
	//basic math
	func testComputationB01() {	test( "1+1=",		"2")		}	//basic math
	func testComputationB02() {	test( "1-2=",		"-1")		}	//basic math
	func testComputationB03() {	test( "2*5=",		"10" )		}	//basic math
	func testComputationB04() {	test( "0*5=",		"0" )		}	//basic math
	func testComputationB05() {	test( "-2-5=",		"-7" )		}	//basic math
	func testComputationB06() {	test( "-2*5=",		"-10" )		}	//basic math
	func testComputationB07() {	test( "-4/2=",		"-2" )		}	//basic math
	func testComputationB09() {	test( "-4/2=",		"-2" )		}	//basic math
	
	//dots
	func testComputationC01() {	test( "0.0000", 	"0.0000")	}	//leading zeros
	func testComputationC02() {	test( ".",			"0")		}	//implicit zero
	func testComputationC03() {	test( ".000",		"0.000")	}	//implicit zero
	func testComputationC04() {	test( "1*.02",		"0.02" )	}	//implicit zero
	func testComputationC05() {	test( ".1",			"0.1" )		}	//implicit zero
	func testComputationC06() {	test( "/3=",		"0" )		}	//implicit zero
	func testComputationC07() {	test( "0.00.01",	"0.0001")	}	//ignoring second dots
	
	//chaining
	func testComputationD01() {	test( "1+1+",		"2" )		}	//subresult
	func testComputationD02() {	test( "1+1+1",		"1" )		}	//subresult
	func testComputationD03() {	test( "1+1+1+",		"3" )		}	//subresult
	func testComputationD04() {	test( "1+1+1+1",	"1" )		}	//subresult
	func testComputationD05() {	test( "1+1+1+1=",	"4" )		}	//subresult
	func testComputationD06() {	test( "1+1+1+1*",	"4" )		}	//subresult
	
	//error
	func testComputationE01() {	test( "1/0=",		"err" )		}	//computation error
	func testComputationE02() {	test( "?",			"err" )		}	//unknown character
	func testComputationE03() {	test( "?1+1",		"err" )		}	//unknown character
	
	//operator change
	func testComputationF01() {	test( "2*-5=",		"-3" )		}	//multiple operators - change previous
	func testComputationF02() {	test( "2*/5=",		"0.4" )		}	//multiple operators - change previous
	func testComputationF03() {	test( "2+-5=",		"-3" )		}	//multiple operators - change previous
	func testComputationF04() {	test( "2-+5=",		"7" )		}	//multiple operators - change previous
	func testComputationF05() {	test( "-2+-5=",		"-7" )		}	//multiple operators - change previous
	func testComputationF06() {	test( "-2--5=",		"-7" )		}	//multiple operators - change previous
	
	//clearing
	func testComputationG01() {	test( "1+2C",		"0" )		}	//clear last number
	func testComputationG02() {	test( "1+2C3",		"3" )		}	//clear last number
	func testComputationG03() {	test( "1+2C3=",		"4" )		}	//clear last number
	func testComputationG04() {	test( "2*2A",		"0" )		}	//clear all
	func testComputationG05() {	test( "2*2A-1=",	"-1" )		}	//clear all
	
	//more advanced operations
	func testComputationH01() {	test( "1+1+1+1*3=",	"12" )		}	//note: calculator is computing left to right (it is not prioritizing multiplication/division above add/sub)
	func testComputationH02() {	test( "2*2+2*2=",	"12" )		}	//note: calculator is computing left to right (it is not prioritizing multiplication/division above add/sub)
	func testComputationH03() {	test( "-1=",		"-1")		}	//negative numbers
	func testComputationH04() {	test( "-1===",		"-3")		}	//repeat last operation
	func testComputationH05() {	test( "1+1=1=",		"1" )		}	//advanced expression
	func testComputationH06() {	test( "1=",			"1" )		}	//advanced expression
	func testComputationH07() {	test( "1==",		"1" )		}	//advanced expression
	func testComputationH08() {	test( "1===",		"1" )		}	//advanced expression
	func testComputationH09() {	test( "5*2=-2C3=",	"7" )		}	//advanced expression
	
	
	private func test(_ input: String, _ expectedOutput: String) {
		
		// GIVEN
		let output: String
		
		
		// WHEN
		do {		output = try processor.process(input: input)	}
		catch _ {	output = "err"									}
		
		
		// THEN
		XCTAssertEqual(output, expectedOutput)
		
	}

}
