//
//  Processor+Enums.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import Foundation


enum OperatorType: Character, CaseIterable {
	case addition		= "+"
	case subtraction	= "-"
	case multiplication	= "*"
	case division		= "/"
	
	var operation: ((_ left: Double, _ right: Double)->(Double)) {
		switch self {
		case .addition			: return { left, right in return left + right }
		case .subtraction		: return { left, right in return left - right }
		case .multiplication	: return { left, right in return left * right }
		case .division			: return { left, right in return left / right }
		}
	}
}



enum CalcSymbolType: Character, CaseIterable {
	case result		= "="
	case clearAll	= "A"
	case clear		= "C"
}



enum ErrorProcessor: Error {
	case noInput
	case unknownInputCharacter
	case computation
}
