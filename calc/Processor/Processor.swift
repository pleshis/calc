//
//  Processor.swift
//  calcProof
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import Foundation


class Processor {

	
	//MARK: - Attributes
	
	public var completionKeyboardHighlight	: (([Character])->())?
	public var completionKeyboardChange		: ((Character, Character)->())?
	
	
	private var leftOperand			: Double				= 0
	private var rightOperand		: Double				= 0
	private var rightOperandPrev	: Double				= 0
	private var operatorType		: OperatorType?			= nil
	private var wasPrevOperator		: Bool					= false
	private var wasPrevResult		: Bool					= false
	private var decimalPlaces		: Int?					= nil
	private var formatter			: CustomNumberFormatter
	
	
	
	//MARK: - General
	
	init(formatter: CustomNumberFormatter? = nil) {
		
		self.formatter = formatter ?? CustomNumberFormatter.beautifying
	}
	
	
	// This method should be used mostly for testing purposes
	@discardableResult
	public func process(input: String) throws -> String {
		
		guard input.count > 0 else {
			throw ErrorProcessor.noInput
		}
		
		var output: String!
		try input.forEach { character in
			output = try process(input: character)
		}
		return output
	}
	
	
	// This method is usually used
	@discardableResult
	public func process(input: Character) throws -> String {
		
		if(input.isDigit){
			
			if(wasPrevResult){
				reset()
			}
			
			let base = 10.0
			if( decimalPlaces == nil ){	// whole numbers
				rightOperand  = rightOperand * base + Double(String(input))!
			}
			else{						// fractions
				decimalPlaces! += 1
				rightOperand  = rightOperand + Double(String(input))! * pow(base, -Double(decimalPlaces!))
			}
			rightOperandPrev = rightOperand
			wasPrevOperator = false
			wasPrevResult	= false
			
			completionKeyboardHighlight?([])
			completionKeyboardChange?("A","C")
			return formatOutput(.right)
			
		}
		else if(input.isOperator){
			
			if((!wasPrevOperator)&&(!wasPrevResult)){
				try computeResult()
			}
			operatorType = OperatorType(rawValue: input)
			wasPrevOperator		= true
			wasPrevResult		= false
			
			completionKeyboardHighlight?( [input] )
			completionKeyboardChange?("A","C")
			return formatOutput(.left)
			
		}
		else if(input.isDot){
			
			if(wasPrevResult){
				reset()
			}
			
			if(decimalPlaces == nil){	//ignore second dot
				decimalPlaces = 0
			}
			wasPrevOperator		= false
			wasPrevResult		= false
			
			completionKeyboardHighlight?([])
			completionKeyboardChange?("A","C")
			return formatOutput(.right)
			
		}
		else if(input.isCalcSymbol){
			
			switch CalcSymbolType(rawValue: input)! {
			case .result:
				
				try computeResult()
				if(operatorType == nil){
					rightOperand = leftOperand	//this is fixing multiple "empty" pressing result button
				}
				
				wasPrevOperator = false
				wasPrevResult	= true
				
				completionKeyboardHighlight?([])
				return formatOutput(.left)
				
				
			case .clear:
				
				clear()
				return formatOutput(.right)
				
				
			case .clearAll:
				
				reset()
				return formatOutput(.right)
				
			}
		}
		
		throw ErrorProcessor.unknownInputCharacter
	}
	
	
	
	//MARK: - Private Methods
	
	private func computeResult() throws {
		
		guard let operatorType = operatorType else {
			leftOperand = rightOperand
			clear()
			return
		}
		
		let result = operatorType.operation(leftOperand, rightOperandPrev)
		if((result.isNaN) || (result.isInfinite)){
			throw ErrorProcessor.computation
		}
		leftOperand = result
		clear()
	}
	
	
	enum ResultType {
		case left
		case right
	}
	
	private func formatOutput(_ resultType: ResultType) -> String {
	
		switch resultType {
		case .left:
			formatter.minimumFractionDigits = 0
			formatter.maximumFractionDigits = 100
			return formatter.string(for: leftOperand)!
			
		case .right:
			if let decimalPlaces = decimalPlaces {
				formatter.minimumFractionDigits = decimalPlaces
				formatter.maximumFractionDigits = decimalPlaces
				formatter.alwaysShowsDecimalSeparator = formatter.forceDecimalSeparator
			}
			else{
				formatter.minimumFractionDigits = 0
				formatter.maximumFractionDigits = 0
				formatter.alwaysShowsDecimalSeparator = false
			}
			return formatter.string(for: rightOperand)!
		}
	}
	
	
	private func clear() {
	
		rightOperand	= 0
		decimalPlaces	= nil
		
		if let operatorType = operatorType {
			wasPrevOperator = true
			completionKeyboardHighlight?( [operatorType.rawValue] )
		}
		else{
			wasPrevOperator = false
			completionKeyboardHighlight?([])
		}
		completionKeyboardChange?("C","A")
	}
	
	
	private func reset() {
		
		clear()
		rightOperandPrev	= 0
		leftOperand			= 0
		operatorType		= nil
		wasPrevOperator		= false
		wasPrevResult		= false
		completionKeyboardHighlight?([])
	}
	
	
}
