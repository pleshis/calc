//
//  ViewController.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit
import SnapKit

class InterfaceViewController: UIViewController {

	
	//MARK: - Attributes
	
	var display		: DisplayView!
	var keyboard	: KeyboardView!
	var lblInfo		: UILabel!
	
	let processor	= Processor()
	
	
	
	//MARK: - General Methods
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupViews()
		
		//bindings
		processor.completionKeyboardHighlight = { [weak self] arrCharacters in
			self?.keyboard.unselectAllKeys()
			arrCharacters.forEach { character in
				self?.keyboard.selectKey(character)
			}
		}
		
		processor.completionKeyboardChange = { [weak self] fromChar, toChar in
			self?.keyboard.changeKey(from: fromChar, to: toChar)
		}
		
		DispatchQueue.main.async {	//device orientation was inderermined
			self.changeOrientation()
		}
		
	}
	
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		changeOrientation()
	}
	
	
	
	//MARK: - Private Methods

	private func setupViews() {
		
		
		// setup
		
		let space: Px = 20
		
		
		// views
		
		let wrapper = UIView()
		view.addSubview(wrapper)
		wrapper.snp.makeConstraints { (make) in
			make.edges.equalTo(view.safeAreaLayoutGuide)
		}
		
		
		display = DisplayView()
		wrapper.addSubview(display)
		display.snp.makeConstraints { (make) in
			make.top.left.right.equalToSuperview().inset(space)
		}
		
		keyboard = KeyboardView(clickBlock: { [weak self] input in
			let output: String
			do {		output = try self!.processor.process(input: input)	}
			catch _ {	output = "error".localize	}
			self?.display.display(output)
		})
		wrapper.addSubview(keyboard)
		keyboard.snp.makeConstraints { (make) in
			make.height.equalTo(display).multipliedBy(4.3)
			make.top.equalTo(display.snp.bottom).offset(space)
			make.left.right.equalTo(display)
			make.width.equalTo(keyboard.snp.height).multipliedBy(0.8).priority(.high)
		}
		
		lblInfo = UILabel()
		wrapper.addSubview(lblInfo)
		lblInfo.snp.makeConstraints { (make) in
			make.centerX.bottom.equalToSuperview()
			make.top.equalTo(keyboard.snp.bottom).offset(space)
			make.height.equalToSuperview().multipliedBy(0.07)
		}
	}
	
	
	private func changeOrientation() {
		
		let isPortrait = UIDevice.current.orientation.isPortrait
		let orientation: KeyboardView.KeyboardOrientation = isPortrait ? .portrait : .landscape
		keyboard.changeOrientation(orientation)
		
		switch orientation {
		case .landscape:
			lblInfo.styleAsInfo("\( "developed by Jan Plesek".localize ) - \( "version:".localize ) \( UIApplication.appFullVersion )")
			
		case .portrait:
			lblInfo.styleAsInfo("\( "version:".localize ) \( UIApplication.appFullVersion )\n\( "developed by Jan Plesek".localize )")
		}
		
	}
	
}

