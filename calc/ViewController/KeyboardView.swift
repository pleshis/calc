//
//  KeyboardView.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit
import SnapKit

class KeyboardView: UIView {
	
	
	
	//MARK: - Setup
	
	private let layoutPortrait = [
		["+", "-", "*", "/"],
		["7", "8", "9", "A"],
		["4", "5", "6", "T"],
		["1", "2", "3", "="],
		["0", "L", ".", "T"],
	]
	
	private let layoutLandscape = [
		["7", "8", "9", "+", "A"],
		["4", "5", "6", "-", "T"],
		["1", "2", "3", "*", "="],
		["0", "L", ".", "/", "T"],
	]
	
	
	
	//MARK: - Enums
	
	enum KeyboardOrientation {
		case landscape
		case portrait
	}
	
	
	
	//MARK: - Attributes
	
	private var arrBtnKeys = [Button]()
	private let clickBlock : (Character)->()
	
	
	
	//MARK: - General Methods
	
	init(clickBlock: @escaping (Character)->()) {
		
		self.clickBlock = clickBlock
		super.init(frame: CGRect.zero)
	}
	
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	
	
	//MARK: - Private Methods
	
	private func reloadLayout(_ layout: [[String]]) {
		
		let arrOldBtnKeys = arrBtnKeys
		arrBtnKeys.forEach { $0.removeFromSuperview() }
		arrBtnKeys.removeAll()
		
		
		let space : Px = 10 * Double( UIFont.multiplier() )
		let maxItemsInRow = layout.map { $0.count }.max()!
		
		var btnLeft		: Button? = nil
		var btnsTop		: [Button?] = (0..<maxItemsInRow).map{ _ in nil }
		
		
		for row in layout {
			for (x, title) in row.enumerated() {
				
				if(title == "L"){
					if let btnLeft = btnLeft {
						if let btnTop = btnsTop[x] {
							btnLeft.snp.makeConstraints({ (make) in
								make.top.equalTo(btnTop.snp.bottom).offset(space)
							})
						}
						btnLeft.consWidth?.deactivate()
						btnLeft.setContentCompressionResistancePriority(.required, for: .horizontal)
					}
					btnsTop[x] = btnLeft
				}
				else if(title == "T"){
					if let btnTop = btnsTop[x] {
						if let btnLeft = btnLeft {
							btnLeft.snp.makeConstraints({ (make) in
								make.right.equalTo(btnTop.snp.left).offset(-space)
								make.width.equalTo(btnTop).priority(.high)
							})
						}
						btnTop.consHeight?.deactivate()
						btnTop.setContentCompressionResistancePriority(.required, for: .vertical)
					}
				}
				else{
					
					
					let btnKey = Button().style(as: ButtonKeyInfo.from(title)!, action: clickBlock)
					arrBtnKeys.append(btnKey)
					addSubview(btnKey)
					btnKey.snp.makeConstraints { (make) in
						make.left.top.greaterThanOrEqualToSuperview()					// stay inside wrapper
						make.right.bottom.lessThanOrEqualToSuperview()					// stay inside wrapper
						make.left.right.top.bottom.equalToSuperview().priority(.high)	// prefer to touch wrapper
						
						if let btnLeft = btnLeft {
							make.left.equalTo(btnLeft.snp.right).offset(space)	// on right from prev left button
							if(btnLeft.isLongLeft){
								make.width.lessThanOrEqualTo(btnLeft)
							}
							else{
								btnKey.consWidth = make.width.equalTo(btnLeft).priority(.high).constraint
							}
							make.height.equalTo(btnLeft).priority(.medium)		// match height
							make.top.equalTo(btnLeft)							// fixing unambiguity
						}
						else{
							btnKey.consWidth = make.width.equalTo(btnKey).constraint	//dummy
						}
						
						if let btnTop = btnsTop[x] {
							make.top.equalTo(btnTop.snp.bottom).offset(space)	// on bottom from prev top button
							if(btnTop.isLongTop){
								make.height.lessThanOrEqualTo(btnTop)
							}
							else{
								btnKey.consHeight = make.height.equalTo(btnTop).priority(.high).constraint
							}
							make.width.equalTo(btnTop).priority(.medium)		//match width
						}
						else{
							btnKey.consHeight = make.height.equalTo(btnKey).constraint	//dummy
						}
					}
					btnsTop[x] = btnKey
					btnLeft	= btnKey
				}
			}
			btnLeft = nil
		}
		
		
		// reload previous states
		if(!arrOldBtnKeys.isEmpty){
			if(arrOldBtnKeys.filter { $0.keyInfo.output == "A" }.isEmpty){	// old array had "C"
				changeKey(from: "A", to: "C")								// change it for new array
			}
			
			arrOldBtnKeys.forEach { btn in
				let character = btn.keyInfo.output
				let btnNew = arrBtnKeys.filter{ $0.keyInfo.output == character }.first
				btnNew?.isSelected = btn.isSelected
			}
		}
		
		
		// no animation
		if(superview != nil){
			self.layoutIfNeeded()
		}
	}
	
	
	
	//MARK: - Accessible Methods
	
	public func unselectAllKeys() {
		
		arrBtnKeys.forEach { btn in
			btn.isSelected = false
		}
	}
	
	
	public func selectKey(_ key: Character) {
		
		arrBtnKeys.filter { btn in
			return btn.keyInfo.output == key
			}.forEach { btn in
				btn.isSelected = true
		}
	}
	
	
	public func changeKey(from fromChar: Character, to toChar: Character) {
		
		arrBtnKeys.forEach { btn in
			if(btn.keyInfo.output == fromChar){
				btn.restyle(to: ButtonKeyInfo.from( String(toChar) )! )
			}
		}
	}
	
	
	public func changeOrientation(_ orientation: KeyboardOrientation) {
		
		let layout : [[String]]
		switch orientation {
		case .portrait:
			layout = layoutPortrait
			
		case .landscape:
			layout = layoutLandscape
		}
		reloadLayout(layout)
	}
	
	
	// this method is used only for testing purposes
	@discardableResult
	public func fakeClick(to character: Character) -> Bool {
		
		var success = false
		arrBtnKeys.forEach { btn in
			if(btn.keyInfo.output == character){
				btn.sendActions(for: .touchUpInside)
				success = true
			}
		}
		return success
	}
	
}
