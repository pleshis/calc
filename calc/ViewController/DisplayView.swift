//
//  DisplayView.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit
import SnapKit

class DisplayView: UIView {

	
	//MARK: - Attributes
	
	private let lblDisplay	: UILabel
	
	
	
	//MARK: - General Methods
	
	init() {
		
		lblDisplay = UILabel().styleAsDisplay()
		lblDisplay.accessibilityLabel = "display result".localize
		super.init(frame: CGRect.zero)
		
		
		let space : Px = 10 * Double( UIFont.multiplier() )
		
		addSubview(lblDisplay)
		lblDisplay.snp.makeConstraints { (make) in
			make.left.right.equalToSuperview().inset(space)
			make.top.bottom.equalToSuperview()
		}
		
		styleWithLine(color: UIColor.my.blue, backAlpha: 0.6)
		display("0")
		
		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
		addGestureRecognizer(tapGesture)
	}
	
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	
	
	//MARK: - User Actions
	
	@objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
		
		var output = lblDisplay.text
		output = output?.replacingOccurrences(of: " ", with: "")	//remove spaces
		UIPasteboard.general.string = output
		
		popMessage( "copied".localize )
	}
	
	
	
	//MARK: - Accessible Methods
	
	public func display(_ input: String) {
		
		lblDisplay.text = input
		lblDisplay.accessibilityValue = input
	}
	
	
	public func popMessage(_ message: String) {
		
		//setup
		let duration	: Sec		= 0.5
		let scaleUp		: CGFloat	= 1.4
		
		
		let lblMessage = UILabel().styleAsSmaller(message)
		addSubview(lblMessage)
		lblMessage.snp.makeConstraints { (make) in
			make.centerY.equalToSuperview().multipliedBy(0.7)
			make.centerX.equalToSuperview()
		}
		
		UIView.animate(withDuration: duration,
					   animations: {
						lblMessage.transform	= CGAffineTransform(scaleX: scaleUp, y: scaleUp)
						lblMessage.alpha		= 0
		}) { _ in
			lblMessage.removeFromSuperview()
		}
	}
	
}
