//
//  UIFont+Style.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit


extension UIFont {
	
	struct my {
		static var bigger	: UIFont {	return UIFont.boldSystemFont(	ofSize: 45 * UIFont.multiplier() )	}
		static var regular	: UIFont {	return UIFont.boldSystemFont(	ofSize: 40 * UIFont.multiplier() )	}
		static var smaller	: UIFont {	return UIFont.boldSystemFont(	ofSize: 24 * UIFont.multiplier() )	}
		static var small 	: UIFont {	return UIFont.systemFont(		ofSize: 16 * UIFont.multiplier() )	}
	}
}
