//
//  TypeDefs.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import Foundation

typealias Px	= Double
typealias Sec	= TimeInterval
