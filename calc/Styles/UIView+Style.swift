//
//  UIView+Style.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit

extension UIView {
	
	@discardableResult
	func styleWithLine(color: UIColor? = nil, backAlpha: CGFloat = 0.8) -> UIView {
		
		layer.cornerRadius	= 12
		layer.borderWidth	= 4
		layer.masksToBounds	= true

		if let color = color {
			layer.borderColor	= color.cgColor
			backgroundColor		= color.withAlphaComponent(backAlpha)
		}
		
		return self
	}
}

