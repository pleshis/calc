//
//  ButtonKeyInfo.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit
import AudioToolbox

enum ButtonKeyInfo {
	
	
	//MARK: - Cases
	
	case digit(_ character: Character)
	case operatorType(_ character: Character)
	case clear(_ character: Character)
	case result
	
	
	
	//MARK: - General Methods
	
	public static func from(_ string: String) -> ButtonKeyInfo? {
		
		guard let char = string.first else { return nil	}
		
		if((char.isDot) || (char.isDigit)){		return .digit(char)			}
		else if(char.isOperator){				return .operatorType(char)	}
		else if(char == "="){					return .result				}
		else if(char.isCalcSymbol){				return .clear(char)			}
		return nil
	}
	
	
	public var display: String {
		
		switch self {
		case .digit(let character):
			if(		character == "0"){	return "0".localize	}
			else if(character == "1"){	return "1".localize	}
			else if(character == "2"){	return "2".localize	}
			else if(character == "3"){	return "3".localize	}
			else if(character == "4"){	return "4".localize	}
			else if(character == "5"){	return "5".localize	}
			else if(character == "6"){	return "6".localize	}
			else if(character == "7"){	return "7".localize	}
			else if(character == "8"){	return "8".localize	}
			else if(character == "9"){	return "9".localize	}
			else if(character == "."){	return ".".localize	}
			return String(character)							//fallback: direct mapping
			
		case .operatorType(let character):
			if(		character == "*"){	return "×".localize	}
			else if(character == "/"){	return "÷".localize	}
			else if(character == "-"){	return "−".localize	}	//special "-" character
			else if(character == "+"){	return "+".localize	}
			return String(character)							//fallback: direct mapping
			
		case .clear(let character):
			if(		character == "A"){	return "AC".localize	}
			else if(character == "C"){	return "C".localize		}
			return String(character)							//fallback: direct mapping
			
		case .result:					return "=".localize
		}
	}
	
	
	// used for acessibility
	public var explain: String {
		
		switch self {
		case .digit(let character):
			if(character.isDigit){
				return "\( "number".localize ) \(character)"
			}
			else if(character.isDot){
				return "fraction dot".localize
			}
			return String(character)							//fallback: direct mapping
		case .operatorType(let character):
			if(character == "*"){	return "multiplication".localize	}
			if(character == "/"){	return "division".localize			}
			if(character == "-"){	return "subtraction".localize		}
			if(character == "+"){	return "addition".localize			}
			return String(character)							//fallback: direct mapping
			
		case .clear(let character):
			if(character == "A"){	return "clear all".localize	}
			if(character == "C"){	return "clear".localize		}
			return String(character)					//fallback: direct mapping
			
		case .result:				return "result".localize
		}
	}
	
	
	// used as input to Processor class
	public var output: Character {
		
		switch self {
		case .digit(let character):			return character
		case .operatorType(let character):	return character
		case .clear(let character):			return character
		case .result:						return "="
		}
	}
	
	
	
	//MARK: - Styling
	
	var color: UIColor {
		
		switch self {
		case .digit(_):			return UIColor.my.gold
		case .operatorType(_):	return UIColor.my.orange
		case .clear(_):			return UIColor.my.red
		case .result:			return UIColor.my.green
		}
	}
	
	
	var colorHighlighted: UIColor {
		
		return color.brighter()
	}
	
	
	var colorSelected: UIColor {
		
		switch self {
		case .digit(_):			return color
		case .operatorType(_):	return UIColor.my.orangeHi
		case .clear(_):			return color
		case .result:			return color
		}
	}
	
	
	var colorSelectedHighlighted: UIColor {
		
		switch self {
		case .digit(_):			return colorSelected.brighter()
		case .operatorType(_):	return colorSelected.brighter()
		case .clear(_):			return colorSelected.brighter()
		case .result:			return colorSelected.brighter()
		}
	}
	
	
	var soundId: SystemSoundID {
		
		let soundIdRaw : Int
		switch self {
		case .digit(_):			soundIdRaw = 1104	// click
		case .operatorType(_):	soundIdRaw = 1105	// clock
		case .clear(_):			soundIdRaw = 1105	// clock
		case .result:			soundIdRaw = 1105	// clock
		}
		
		return SystemSoundID( soundIdRaw )
	}
	
	
}
