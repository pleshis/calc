//
//  UIColor+Style.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit

extension UIColor {

	struct my {
		static var red		: UIColor {		return	UIColor(hex: 0xFF3A30)	}
		static var orange	: UIColor {		return	UIColor(hex: 0xF4851D)	}
		static var orangeHi	: UIColor {		return	UIColor(hex: 0xFF7044)	}
		static var gold		: UIColor {		return	UIColor(hex: 0xFFB400)	}
		static var blue		: UIColor {		return	UIColor(hex: 0x157EFB)	}
		static var green	: UIColor {		return	UIColor(hex: 0x7FB800)	}
		static var white	: UIColor {		return	UIColor(hex: 0xF9F9F9)	}
		static var black	: UIColor {		return	UIColor(hex: 0x444444)	}
	}
}

