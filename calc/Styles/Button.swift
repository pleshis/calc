//
//  Button.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit
import SnapKit
import AudioToolbox

class Button: UIButton {
	
	
	//MARK: - Setup
	
	let animationDuration	: Sec		= 0.15
	let scaleUp				: CGFloat	= 1.1
	
	
	
	//MARK: - Attributes
	
	private var action			: ((Button)->())? = nil
	private(set) var keyInfo	: ButtonKeyInfo!
	
	var consHeight	: Constraint? = nil
	var consWidth	: Constraint? = nil
	
	var isLongTop	: Bool {	return !(consHeight?.isActive	?? true)	}
	var isLongLeft	: Bool {	return !(consWidth?.isActive	?? true)	}
	
	
	
	//MARK: - General Methods
	
	@discardableResult
	public func style(as keyInfo: ButtonKeyInfo, action: @escaping (Character)->()) -> Button {
		
		self.action = { [weak self] _ in action( self!.keyInfo.output ) }
		titleLabel?.font = UIFont.my.regular
		setTitleColor(UIColor.my.white, for: .normal)
		restyle(to: keyInfo)
		
		// actions
		addTarget(self, action: #selector(click(_:)),		for: .touchUpInside)
		addTarget(self, action: #selector(touchDown(_:)),	for: .touchDown)
		addTarget(self, action: #selector(touchUp(_:)),		for: .touchDragOutside)
		addTarget(self, action: #selector(touchDown(_:)),	for: .touchDragInside)
		
		return self
	}
	
	
	public func restyle(to keyInfo: ButtonKeyInfo) {
		
		self.keyInfo = keyInfo
		
		styleWithLine(color: UIColor.my.black.withAlphaComponent(0.1),
					  backAlpha: 0)
		
		setTitle(keyInfo.display, for: .normal)
		
		setBackgroundImage(keyInfo.color.image(), 					for: .normal)
		setBackgroundImage(keyInfo.colorHighlighted.image(),		for: .highlighted)
		setBackgroundImage(keyInfo.colorSelected.image(),			for: .selected)
		setBackgroundImage(keyInfo.colorSelectedHighlighted.image(),for: [.selected, .highlighted])
		
		accessibilityLabel = keyInfo.explain
	}
	
	
	
	//MARK: - User Actions
	
	@objc func click(_ sender: Button) {
		action?(sender)
		touchUp(sender)
		
		AudioServicesPlaySystemSound( keyInfo!.soundId )
	}
	
	
	@objc func touchDown(_ sender: Button) {
		UIView.animate(withDuration: animationDuration) {
			sender.transform = CGAffineTransform(scaleX: self.scaleUp, y: self.scaleUp)
		}
	}
	
	
	@objc func touchUp(_ sender: Button) {
		UIView.animate(withDuration: animationDuration) {
			sender.transform = CGAffineTransform.identity
		}
	}
	
	
	
}
