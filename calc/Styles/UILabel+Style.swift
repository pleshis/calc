//
//  UILabel+Style.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit

extension UILabel {

	
	@discardableResult
	func styleAsDisplay() -> UILabel {
		
		textColor		= UIColor.my.white
		font			= UIFont.my.bigger
		textAlignment	= .right
		adjustsFontSizeToFitWidth = true
		return self
	}
	
	
	@discardableResult
	func styleAsSmaller(_ text: String) -> UILabel {
		
		self.text		= text
		textColor		= UIColor.my.white
		font			= UIFont.my.smaller
		return self
	}
	
	
	@discardableResult
	func styleAsInfo(_ text: String) -> UILabel {
		
		self.text		= text
		font			= UIFont.my.small
		textColor		= UIColor.my.black
		textAlignment	= .center
		numberOfLines	= 0
		return self
	}
	
}
