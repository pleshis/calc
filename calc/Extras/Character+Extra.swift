//
//  Character+Extra.swift
//  calcProof
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import Foundation


extension Character {
	
	
	public var isDigit: Bool {
		return CharacterSet.decimalDigits.contains(self)
	}
	
	
	public var isOperator: Bool {
		let strOperators = OperatorType.allCases.map { "\($0.rawValue)" }.joined()
		return CharacterSet(charactersIn: strOperators).contains(self)
	}
	
	
	public var isDot: Bool {
		return (self == ".")
	}
	
	
	public var isCalcSymbol: Bool {
		let strCalcSymbol = CalcSymbolType.allCases.map { "\($0.rawValue)" }.joined()
		return CharacterSet(charactersIn: strCalcSymbol).contains(self)
	}
	
}

