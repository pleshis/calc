//
//  UIColor+Extra.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit

extension UIColor {
	
	public convenience init(hex: UInt32) {
		self.init(
			red:   CGFloat(Double((hex & 0xFF0000) >> 16) / 255.0),
			green: CGFloat(Double((hex & 0x00FF00) >> 8)  / 255.0),
			blue:  CGFloat(Double((hex & 0x0000FF))		  / 255.0),
			alpha: 1.0
		)
	}
	
	
	public func image(of size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
		
		let rect = CGRect(origin: CGPoint.zero, size: size)
		UIGraphicsBeginImageContext(size)
		let context = UIGraphicsGetCurrentContext()
		
		context?.setFillColor(self.cgColor)
		context?.fill(rect)
		
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return image!
	}
	
	
	public func darker(diff:CGFloat = 0.25) -> UIColor {
		return self.changeBrightness(-diff)
	}
	
	
	public func brighter(diff:CGFloat = 0.25) -> UIColor {
		return self.changeBrightness(+diff)
	}
	
	
	private func changeBrightness(_ ratio:CGFloat) -> UIColor {
		
		var h: CGFloat = 0
		var s: CGFloat = 0
		var b: CGFloat = 0
		var a: CGFloat = 0
		
		guard getHue(&h, saturation: &s, brightness: &b, alpha: &a) else {
			return self
		}
		return UIColor(hue:			h,
					   saturation:	s,
					   brightness:	b * (1.0 + ratio),
					   alpha:		a)
	}
}
