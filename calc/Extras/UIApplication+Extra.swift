//
//  UIApplication+Extra.swift
//  calc
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit

extension UIApplication {
	
	
	static var appVersion: String? {
		return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
	}
	
	
	static var appBuild: String? {
		return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
	}
	
	
	static var appFullVersion: String {
		return "\(UIApplication.appVersion!)(\(UIApplication.appBuild!))"
	}
}
