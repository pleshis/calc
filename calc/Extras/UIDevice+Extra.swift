//
//  UIDevice+Extra.swift
//  calc
//
//  Created by Jan Plesek on 28/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit


//MARK: - Enums

public enum DeviceModel : CGFloat {
	case iPhone3Size		=  480	//iPhone 2G or 3G or 3GS
	case iPhone4Size		=  960	//iPhone 4 or 4S
	case iPhone5Size		= 1136	//iPhone 5 or 5S or 5C or 5SE
	case iPhone6Size		= 1334	//iPhone 6/6S/7/8
	case iPhonePlusSizeDev	= 1920	//iPhone 6+/6S+/7+/8+	(rendered to 2208)	- device
	case iPhonePlusSizeSim	= 2208	//iPhone 6+/6S+/7+/8+	(rendered to 2208)	- simulator
	case iPhoneXSize		= 2436	//iPhone X
	case iPhoneXMaxSize		= 2688	//iPhone X Max
	
	case iPadMiniSize		= 2048	//iPad Mini 7.9"
//	case iPadAirSize		= 2048	//iPad Air 9.7" -  has the same resolution as iPad Mini
	case iPadPro10Size		= 2224	//iPad 10.5" Pro
	case iPadPro11Size		= 2388	//iPad 11.0" Pro
	case iPadPro13Size		= 2732	//iPad 12.9" Pro
}


extension UIDevice {
	
	
	//MARK: - Accessible Methods
	
	public static func phoneModel() -> DeviceModel? {
		
		let idiom = UIDevice().userInterfaceIdiom
		if(idiom == .phone) {
			return DeviceModel(rawValue: UIScreen.main.nativeBounds.height)
		}
		if(idiom == .pad) {
			let maxSide = max(UIScreen.main.nativeBounds.height, UIScreen.main.nativeBounds.width)
			return DeviceModel(rawValue: maxSide)
		}
		return nil
	}
	
	
	public static func isiPhoneX() -> Bool {
		
		return (phoneModel() == DeviceModel.iPhoneXSize)
	}
	
	
	public static func isSmallSize() -> Bool {
		
		let phoneModel = self.phoneModel()
		return ((phoneModel == DeviceModel.iPhone3Size) ||
				(phoneModel == DeviceModel.iPhone4Size) ||
				(phoneModel == DeviceModel.iPhone5Size) )
	}
	
	
	public static func isNormalSize() -> Bool {
		
		let phoneModel = self.phoneModel()
		return ((phoneModel == DeviceModel.iPhone6Size) ||
				(phoneModel == DeviceModel.iPhoneXSize) )
	}
	
	
	public static func isPlusSize() -> Bool {
		
		let phoneModel = self.phoneModel()
		return ((phoneModel == DeviceModel.iPhonePlusSizeDev) ||
				(phoneModel == DeviceModel.iPhonePlusSizeSim) )
	}
	
	
	public static func isMaxSize() -> Bool {
		
		return (phoneModel() == DeviceModel.iPhoneXMaxSize)
	}
}
