//
//  String+Extra.swift
//  calc
//
//  Created by Jan Plesek on 28/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import Foundation

extension String {
	
	public var localize: String {
		return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
	}
	
}

