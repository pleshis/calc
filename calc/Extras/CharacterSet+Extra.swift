//
//  CharacterSet+Extra.swift
//  calcProof
//
//  Created by Jan Plesek on 27/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import Foundation


extension CharacterSet {
	
	public func contains(_ c: Character) -> Bool {
		
		return contains(c.unicodeScalars.first!)
	}
}
