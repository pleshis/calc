//
//  CustomNumberFormatter.swift
//  calc
//
//  Created by Jan Plesek on 28/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import Foundation

class CustomNumberFormatter: NumberFormatter {
	
	
	//MARK: - Attributes
	
	var forceDecimalSeparator: Bool = false
	
	
	
	//MARK: - Class Methods
	
	public static var beautifying: CustomNumberFormatter {
		
		let formatter = CustomNumberFormatter()
		formatter.numberStyle = .decimal
		formatter.minimumIntegerDigits = 1
		formatter.groupingSize = 3
		formatter.groupingSeparator = " "
		formatter.usesGroupingSeparator = true
		formatter.forceDecimalSeparator = true
		return formatter
	}
	
	
	public static var testing: CustomNumberFormatter {
		
		let formatter = CustomNumberFormatter()
		formatter.minimumIntegerDigits = 1
		formatter.usesGroupingSeparator = false
		formatter.forceDecimalSeparator = false
		return formatter
	}
	
}
