//
//  UIFont+Extra.swift
//  calc
//
//  Created by Jan Plesek on 28/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import UIKit

extension UIFont {
	
	public static func multiplier() -> CGFloat {
		
		//or could be based on iPhone Size (small, normal, plus)
		guard let phoneSize = UIDevice.phoneModel() else {
			return 1
		}
		switch phoneSize {
		case .iPhone3Size,
			 .iPhone4Size:
			return 0.8
			
		case .iPhone5Size:
			return 0.9
			
		case .iPhone6Size:
			return 0.9
			
		case .iPhoneXSize:
			return 1.0
			
		case .iPhonePlusSizeDev,
			 .iPhonePlusSizeSim:
			return 1.1
			
		case .iPhoneXMaxSize:
			return 1.15
			
		case .iPadMiniSize:
			return 1.8
			
		case .iPadPro10Size:
			return 1.9
			
		case .iPadPro11Size:
			return 2.1
			
		case .iPadPro13Size:
			return 2.3
			
		}
	}
}
