//
//  keyboardUITests.swift
//  keyboardUITests
//
//  Created by Jan Plesek on 28/04/2019.
//  Copyright © 2019 Jan Plesek. All rights reserved.
//

import XCTest
@testable import calc


class keyboardUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }
	
	
	// inputs test
	func testFlow01() {
		
		let app = XCUIApplication()
		let display = { return app.staticTexts["display result"].value as! String }
		
		
		app.buttons["number 9"].tap()
		XCTAssert(display() == "9")
		
		app.buttons["number 8"].tap()
		XCTAssert(display() == "98")
		
		app.buttons["number 7"].tap()
		XCTAssert(display() == "987")
		
		app.buttons["number 6"].tap()
		XCTAssert(display() == "9 876")
		
		app.buttons["number 5"].tap()
		XCTAssert(display() == "98 765")
		
		app.buttons["number 4"].tap()
		XCTAssert(display() == "987 654")
		
		app.buttons["number 3"].tap()
		XCTAssert(display() == "9 876 543")
		
		app.buttons["number 2"].tap()
		XCTAssert(display() == "98 765 432")
		
		app.buttons["number 1"].tap()
		XCTAssert(display() == "987 654 321")
		
		app.buttons["number 0"].tap()
		XCTAssert(display() == "9 876 543 210")
		
		app.buttons["fraction dot"].tap()
		XCTAssert(display() == "9 876 543 210.")
		
		app.buttons["number 0"].tap()
		XCTAssert(display() == "9 876 543 210.0")
		
		
		app.buttons["addition"].tap()
		XCTAssert(app.buttons["addition"].isSelected)
		
		app.buttons["subtraction"].tap()
		XCTAssert(app.buttons["subtraction"].isSelected)
		
		app.buttons["multiplication"].tap()
		XCTAssert(app.buttons["multiplication"].isSelected)
		
		app.buttons["division"].tap()
		XCTAssert(app.buttons["division"].isSelected)
		
		
		app.buttons["clear"].tap()
		app.buttons["clear all"].tap()
		XCTAssert(display() == "0")
		
	}
	
	
	// basic math
	func testFlow02() {
		
		let app = XCUIApplication()
		let display = { return app.staticTexts["display result"].value as! String }
		
		app.buttons["number 1"].tap()
		XCTAssert(display() == "1")
		
		let btnPlus = app.buttons["addition"]
		btnPlus.tap()
		XCTAssert(btnPlus.isSelected)
		XCTAssert(display() == "1")
		
		app.buttons["number 2"].tap()
		XCTAssert(!btnPlus.isSelected)
		XCTAssert(display() == "2")
		
		app.buttons["result"].tap()
		XCTAssert(display() == "3")
		
	}
	
	
	// advanced expression
	func testFlow03() {
		
		let app = XCUIApplication()
		let display = { return app.staticTexts["display result"].value as! String }
		
		app.buttons["number 5"].tap()
		XCTAssert(display() == "5")
		
		let btnTimes = app.buttons["multiplication"]
		btnTimes.tap()
		XCTAssert(btnTimes.isSelected)
		XCTAssert(display() == "5")
		
		app.buttons["number 2"].tap()
		XCTAssert(!btnTimes.isSelected)
		XCTAssert(display() == "2")
		
		app.buttons["result"].tap()
		XCTAssert(display() == "10")
		
		let btnMinus = app.buttons["subtraction"]
		btnMinus.tap()
		XCTAssert(btnMinus.isSelected)
		XCTAssert(display() == "10")
		
		app.buttons["number 2"].tap()
		XCTAssert(!btnMinus.isSelected)
		XCTAssert(display() == "2")
		
		app.buttons["clear"].tap()
		XCTAssert(btnMinus.isSelected)
		XCTAssert(display() == "0")
		
		app.buttons["number 3"].tap()
		XCTAssert(!btnMinus.isSelected)
		XCTAssert(display() == "3")
		
		app.buttons["result"].tap()
		XCTAssert(display() == "7")
		
	}
	
	
	// clipboard
	func testFlow04() {
		
		let app = XCUIApplication()
		let display = { return app.staticTexts["display result"].value as! String }
		
		app.buttons["number 9"].tap()
		XCTAssert(display() == "9")
		
		app.staticTexts["display result"].tap()
		
		XCTAssert(UIPasteboard.general.string == "9")
		
	}
	
	
	// clear all button
	func testFlow05() {
		
		let app = XCUIApplication()
		let display = { return app.staticTexts["display result"].value as! String }
		
		XCTAssertTrue( app.buttons["clear all"].exists)
		XCTAssertFalse(app.buttons["clear"].exists)
		
		
		app.buttons["number 1"].tap()
		XCTAssert(display() == "1")
		XCTAssertTrue( app.buttons["clear"].exists)
		XCTAssertFalse(app.buttons["clear all"].exists)
		
		let btnPlus = app.buttons["addition"]
		btnPlus.tap()
		XCTAssert(btnPlus.isSelected)
		XCTAssert(display() == "1")
		
		app.buttons["number 2"].tap()
		XCTAssert(!btnPlus.isSelected)
		XCTAssert(display() == "2")
		
		app.buttons["clear"].tap()
		XCTAssert(btnPlus.isSelected)
		XCTAssert(display() == "0")
		XCTAssertTrue( app.buttons["clear all"].exists)
		XCTAssertFalse(app.buttons["clear"].exists)
		
		app.buttons["clear all"].tap()
		XCTAssertTrue( app.buttons["clear all"].exists)
		XCTAssertFalse(app.buttons["clear"].exists)
		XCTAssert(display() == "0")
		
	}

}
